## Projeto2_ED
Projeto 2 Estrutura de Dados 1

# Comando de compilação

gcc trab2.c -Wall -o test.out -lm

# Passo a passo para executar programa

1 - Compile com o comando acima;
2 - Adicione os arquivos ".txt" do DataSet na mesma pasta em que os arquivos do programa estiver;
3 - Execute o programa;
4 - Para executar novamente exclua os arquivos "blacklist-asfalto.txt" e "blacklist-grama.txt";


