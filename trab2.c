/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nomes: Bruno Carmo Nunes 18/0117548
 *        Guilherme Dourado 16/0123020
 *        Guilherme Peixoto 17/0034941
 * Trabalho 2
*/

#include "ILBP.h"
#include "GLCM.h"
#include <string.h>

typedef struct vet_carac {
	
	double vetor_img[536];

} VETOR;

int main(int argc, char *argv[]){

    
    VETOR asfalto[50]; 
    VETOR grama[50];
    VETOR treino_grama;
    VETOR treino_asfalto;
    double resultado_asfalto, resultado_grama;
    float TA = 0.0f,TFA = 0.0f,TFR = 0.0f;

// Para treinamento da imgagem
    for(int k=0; k<25; k++){
    	
	gerarVetorILBP(asfalto[k].vetor_img, grama[k].vetor_img);
     	normalizarVetor(asfalto[k].vetor_img);
    	normalizarVetor(grama[k].vetor_img);

	}

    //Vetores na posicao 0 a 24 sao de treinamento, o restanto sao teste
    
    //Media de todos os vetores e inseridos em treino para cada grupo de img
    for(int i=0;i<25;i++){
        for(int j=0;j<536;j++){
            
        treino_grama.vetor_img[j] += (grama[i].vetor_img[j] / 25);
        treino_asfalto.vetor_img[j] += (asfalto[i].vetor_img[j] / 25);

        }
  }


//Para Teste da imagem
    for(int k=25; k<50; k++){
    	
	gerarVetorILBP(asfalto[k].vetor_img, grama[k].vetor_img);
    	normalizarVetor(asfalto[k].vetor_img);
    	normalizarVetor(grama[k].vetor_img);
    }  

//Laco para fazer a distancia euclidiana dos treinamentos com os vetores grama    

	resultado_grama = 0;
	resultado_asfalto = 0;

for(int j=25; j<50; j++){
    for(int i=0; i<536; i++){

     resultado_grama += pow((treino_grama.vetor_img[i] - grama[j].vetor_img[i]),2);  
     resultado_asfalto += pow((treino_asfalto.vetor_img[i] - grama[j].vetor_img[i]),2);
    }
     resultado_grama = sqrt(resultado_grama);
     resultado_asfalto = sqrt(resultado_asfalto);

     if(resultado_grama < resultado_asfalto) TA += 1;
     else TFR += 1;
}

//Laco para fazer a distacia euclidiana dos treinamentos com os vetores asfalto

     resultado_grama = 0;
     resultado_asfalto = 0;

for(int j=25; j<50; j++){
    for(int i=0; i<536; i++){

     resultado_grama += pow((treino_grama.vetor_img[i] - asfalto[j].vetor_img[i]),2);  
     resultado_asfalto += pow((treino_asfalto.vetor_img[i] - asfalto[j].vetor_img[i]),2);
    }
     resultado_grama = sqrt(resultado_grama);
     resultado_asfalto = sqrt(resultado_asfalto);

     if(resultado_grama > resultado_asfalto) TA += 1;
     else TFA += 1;
}

//Resultados apresentados na tela

	printf("Taxa de acerto: %.2f%% \n",(TA/50)*100);
	printf("Taxa de falsa aceitacao: %.2f%% \n",(TFA/50)*100);
	printf("Taxa de falsa rejeicao: %.2F%% \n",(TFR/50)*100);

     putchar('\n');

    // executaGLCM();
   
    return 0;

} 
