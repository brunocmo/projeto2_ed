/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nomes: Bruno Carmo Nunes 18/0117548
 *        Guilherme Dourado 16/0123020
 *        Guilherme Peixoto 17/0034941
 * Trabalho 2
*/

#ifndef GLCM_H
#define GLCM_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define SIZE_OF_GLCM_MATRIX 256
#define SIZE_OF_IMAGE 1024
#include "misc.h"
struct Imagens_e_funcoes{
    //[0],[1],[2],[3],[4],[5],[6],[7]
    // N , NE, E, SE , S, SW,  W,  NW
    double **Matriz_imagem;	    
    
    double **glcm[7]; 
    
    double **LOCALIZACAO_GLCM_ALOCADA[7],
           **LOCALIZACAO_GLCM_ALTERADA[7],    
           *LOCALIZACAO_summ[7]; 
    
    double *summ[7],
           *energy[7],
           *contrast[7], 
           *homogeneity[7]; 
           
    double *vector[7]; 
};

double **aloca_matriz_glcm(double **glcm_LOC){    	
    glcm_LOC = (double**) malloc(SIZE_OF_GLCM_MATRIX*sizeof(double *)); 

    for(int i = 0 ; i < SIZE_OF_GLCM_MATRIX ; i++){
            *(glcm_LOC+i) = (double *) malloc(SIZE_OF_GLCM_MATRIX*sizeof(double)); 
    }
    return glcm_LOC;
}


double **GLCM(double **matriz_imagem,double** Matriz_glcm,int pixel_vizinho,int pixel_central){           
    int linha;
    int coluna;
    
    for(int i = 1 ; i < SIZE_OF_IMAGE-1; i++){
        for(int j = 1 ; j < SIZE_OF_IMAGE-1; j++){
            linha = matriz_imagem[i][j];
            coluna = matriz_imagem[i+(pixel_vizinho)][j+(pixel_central)]; 
            Matriz_glcm[linha][coluna]+=1;
        //    printf("%.0lf ", Matriz_glcm[linha][coluna]); 
        }
        //printf("\n");
    
    }
    return Matriz_glcm;
}



//Se algo no codigo der errado e culpa do summ... SAPORCARIA
double *summ_glcm(double**glcm, double *summ){
    *summ = 0;  
    for(int i = 0 ; i < SIZE_OF_GLCM_MATRIX ; i++){
        for(int j = 0 ; j < SIZE_OF_GLCM_MATRIX ; j++){
            *summ += glcm[i][j]; 
         }
    } 
  //  printf(" \nSUMM = %lf\n",*summ );
    return summ; 
}

int *co_ocurrence(double** co_ocurrence_glcm ,double *summ){
    for(int i = 0 ; i < SIZE_OF_GLCM_MATRIX ; i ++){
        for(int j = 0 ; j < SIZE_OF_GLCM_MATRIX ; j++){
            co_ocurrence_glcm[i][j]=co_ocurrence_glcm[i][j]/(*summ); 
            //printf("%.5lf ", co_ocurrence_glcm[i][j]); 
        }
//        printf("\n");
    }
    return 0; 
}

double *energy_co_ocurrence(double**glcm_co_ocurrence, double *energy){
        *energy = 0; 
        for(int i = 0 ; i < SIZE_OF_GLCM_MATRIX ; i++){
            for(int j = 0 ; j < SIZE_OF_GLCM_MATRIX ; j++){
                *energy+= pow(glcm_co_ocurrence[i][j],2);
            }
        }  
        //printf("Energy = %lf \n", *energy); 
    
        return energy; 
}

double *contrast_co_ocurrence(double **glcm_co_ocurrence, double *contrast){
        *contrast = 0;    
        
        for(int i = 0 ; i < SIZE_OF_GLCM_MATRIX ; i++){
            for(int j = 0 ; j < SIZE_OF_GLCM_MATRIX ; j++){
                *contrast += pow(abs(i-j),2)*glcm_co_ocurrence[i][j];
            }
        }
        //printf("Contrast = %lf\n", *contrast); 
        return contrast; 
}

double *homogeneity_co_ocurrence(double **glcm_co_ocurrence , double *homogeneity){
        *homogeneity = 0; 
        for(int i = 0 ; i < SIZE_OF_GLCM_MATRIX ; i++){
            for(int j = 0 ; j < SIZE_OF_GLCM_MATRIX ; j++){
                    *homogeneity += (glcm_co_ocurrence[i][j])/(1+abs(i-j)); 
            }
        }
        //printf("homogeneity = %lf\n", *homogeneity); 

        return homogeneity; 
}

double *vector_functions(double *energy,double *contrast,double *homogeneity , double *vector){
        vector[0] = *energy; 
        vector[1] = *contrast; 
        vector[2] = *homogeneity;
        
       // printf("energy1 = %lf\n", vector[0]);
       // printf("contrast1 = %lf\n" , vector[1]);
       // printf("homogeneity1 = %lf\n" , vector[2]); 
        
        return vector; 
}

double *GLCM_main(int *Matriz_mae){
    int loc_x[8] = {0,+1,+1,+1,0,-1,-1,-1},
        loc_y[8] = {-1,-1,0,+1,+1,+1,0,-1};
    
    struct Imagens_e_funcoes imagens_e_funcoes[50]; 
  ////////////////////////////////////////THIS PART IS ONLY TO DEBUG//////////////////////////////////
    imagens_e_funcoes[0].Matriz_imagem = (double **) malloc(SIZE_OF_IMAGE*sizeof(double *));                  
    for(int i = 0 ; i< SIZE_OF_IMAGE; i++){                                                           //
	*(imagens_e_funcoes[0].Matriz_imagem+i) = (double*) malloc(SIZE_OF_IMAGE*sizeof(double));	
    }                                                                                                 //

    for(int i = 0 ; i < SIZE_OF_IMAGE ; i++){                                                         //
    	for(int j = 0 ; j < SIZE_OF_IMAGE ; j++){
		
        imagens_e_funcoes[0].Matriz_imagem[i][j] = *(Matriz_mae+ (i * colunas) + j);
        //printf("-> %lf", imagens_e_funcoes[0].Matriz_imagem[i][j]);
        //scanf("%lf", &imagens_e_funcoes[0].Matriz_imagem[i][j]);                                    //
		//getchar();
		//Por que armazeno a variavel no endereco ????????? e nao no ponteiro ? PREOLA                //
		//Talvez por que **A = A[size][size] != ***A 
	}	                                                                                              //
    }
   /////////////////////////////////////////////////////////////////////////////////////////////////////
    

    
    //TALVEZ TIRAR ESSA PARTE :p 
   for(int i = 0 ; i < 8 ; i++){
        *(imagens_e_funcoes[0].summ+i)= (double*) malloc(sizeof(double)); 
        *(imagens_e_funcoes[0].LOCALIZACAO_summ+i)= (double*) malloc(sizeof(double)); 
        *(imagens_e_funcoes[0].energy+i) = (double*) malloc(sizeof(double)); 
        *(imagens_e_funcoes[0].contrast+i) = (double*) malloc(sizeof(double)); 
        *(imagens_e_funcoes[0].homogeneity+i) = (double*) malloc(sizeof(double)); 
        *(imagens_e_funcoes[0].vector+i) = (double * ) malloc (4*sizeof(double));
    }
    
   double *Vetores_finais;
    
    Vetores_finais = (double*) malloc(25*sizeof(double));
    
    
    
    //imagens_e_funcoes[0].summ = (double*) malloc(sizeof(double *)); 
    for(int i = 0 ; i < 8 ; i++){
        imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALOCADA[i] = aloca_matriz_glcm(imagens_e_funcoes[0].glcm[i]);
        
        imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALTERADA[i] = GLCM(imagens_e_funcoes[0].Matriz_imagem,imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALOCADA[i],loc_x[i],loc_y[i]);
        
    imagens_e_funcoes[0].LOCALIZACAO_summ[i] = summ_glcm(imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALTERADA[i],imagens_e_funcoes[0].summ[i]); 
        
        co_ocurrence(imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALTERADA[i],imagens_e_funcoes[0].LOCALIZACAO_summ[i]);
        energy_co_ocurrence(imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALTERADA[i] , imagens_e_funcoes[0].energy[i]); 
        
        contrast_co_ocurrence(imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALTERADA[i], imagens_e_funcoes[0].contrast[i]);
        homogeneity_co_ocurrence(imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALTERADA[i],imagens_e_funcoes[0].homogeneity[i]); 
        
        vector_functions(imagens_e_funcoes[0].energy[i],imagens_e_funcoes[0].contrast[i],imagens_e_funcoes[0].homogeneity[i], imagens_e_funcoes[0].vector[i]); 
        
        
        Vetores_finais[3*i+0] = imagens_e_funcoes[0].vector[i][0];
        Vetores_finais[3*i+1] = imagens_e_funcoes[0].vector[i][1];
        Vetores_finais[3*i+2] = imagens_e_funcoes[0].vector[i][2];
        
        //printf("energy1 = %lf\n", Vetores_finais[3*i+0]);
        //printf("contrast1 = %lf\n" , Vetores_finais[3*i+1]);
        //printf("homogeneity1 = %lf\n" , Vetores_finais[3*i+2]); 
        
    
        //libera_matriz_glcm(imagens_e_funcoes[0].glcm[i]); 
        //libera_funcoes(imagens_e_funcoes[0].summ[i], imagens_e_funcoes[0].LOCALIZACAO_summ [i],imagens_e_funcoes[0].energy[i],imagens_e_funcoes[0].contrast[i],imagens_e_funcoes[0].homogeneity[i],imagens_e_funcoes[0].vector[i]); 
        
        
        /*for(int il = 0 ; il < SIZE_OF_GLCM_MATRIX; il++){
            free(imagens_e_funcoes[0].glcm[i]);
            if(il == SIZE_OF_GLCM_MATRIX-1) free(imagens_e_funcoes[0].glcm); 
        }*/

        
       /* printf("\nenergy_vector = %lf\n", Vetores_finais[i][0]);
        printf("\ncontrast_vector = %lf\n", Vetores_finais[i][1]);
        printf("\nhomogeneity_vector = %lf\n", Vetores_finais[i][2]);*/
    }
    
    

    //////////////////////////////////////THIS PART IS ONLY TO DEBUG PRINT/////////////////////////////////
   //
 //   for(int matriz_glcm_POS = 0 ; matriz_glcm_POS < 8 ; matriz_glcm_POS++){
  //      for(int i = 0 ; i < SIZE_OF_GLCM_MATRIX; i ++){                                            //
        //printf("\n"); 
    //        for(int j = 0 ; j < SIZE_OF_GLCM_MATRIX; j++){                                             
                //printf("%0.03lf ", imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALTERADA[matriz_glcm_POS][i][j]);
     //       }                                                                                    //       
        //printf("\n");
  //      }   
      //  printf("\n");
  //  }
    //////////////////////////////////////////////////////////////////////////////////////////////////////

    //puts("");

    return Vetores_finais; 
}

#endif
